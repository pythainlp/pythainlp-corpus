# PyThaiNLP Corpus
[![Build Status](https://travis-ci.org/PyThaiNLP/pythainlp-corpus.svg?branch=2.1)](https://travis-ci.org/PyThaiNLP/pythainlp-corpus)

Corpora and language models for [PyThaiNLP](https://github.com//PyThaiNLP/pythainlp).

- All corpora, datasets, and documentation created by PyThaiNLP project are released under [Creative Commons Zero 1.0 Universal Public Domain Dedication License](https://creativecommons.org/publicdomain/zero/1.0/) (CC0).
- All language models created by PyThaiNLP project are released under [Creative Commons Attribution 4.0 International Public License](https://creativecommons.org/licenses/by/4.0/) (CC-by).
- For other corpora that may included with PyThaiNLP distribution (for example, when you `pip install pythainlp`), please refer to PyThaiNLP module's [Corpus License](https://github.com/PyThaiNLP/pythainlp/blob/dev/pythainlp/corpus/corpus_license.md).

## Branches

- `master` for test and dev
- `2.2` for PyThaiNLP 2.2
- `2.1` for PyThaiNLP 2.1
- `2.0` for PyThaiNLP 2.0
- `1.7` for PyThaiNLP 1.7
